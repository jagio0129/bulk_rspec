### 概要

HEADから指定commitまでのrspec差分ファイルのみをテストするスクリプト

### 導入

```sh
cd ~
git clone https://gitlab.com/jagio0129/bulk_rspec.git
cat 'alias bulk_rspec="bundle exec ruby ~/dotfiles/bulk_rspec.rb"' >> ~/.bashrc
exec $SHELL -l
```

### 使い方

```sh
# 引数なし = HEAD^
bulk_rspec

# origin/masterからHEADまでで変更差分があった*_spec.rbのテストを実行する
#   HEADがorigin/masterから伸びていること前提
bulk_rspec origin/master

# zeusでテストを回したいとき
buck_rspec origin/master -z
```
